import React, { useState } from "react";
import Swal from "sweetalert2";

function TodoForm({ addTodo }) {
  const [value, setValue] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (value) {
      Swal.fire(
        "เพิ่มข้อมูลสำเร็จ",
        "The data was successful again.",
        "success"
      );
      setTimeout(() => {
        Swal.close();
      }, 2000);
      addTodo(value);
      setValue("");
    } else {
      Swal.fire("กรุณากรอกข้อมูล", "Please fill in information", "error");
      setTimeout(() => {
        Swal.close();
      }, 3000);
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit} className="TodoForm">
        <input
          type="text"
          value={value}
          onChange={(e) => setValue(e.target.value)}
          className="todo-input"
          placeholder="เพิ่มรายละเอียด"
        />
        <button
          type="submit"
          className="todo-btn hover:bg-white hover:text-purple-700 "
        >
          AddTask
        </button>
      </form>
    </div>
  );
}

export default TodoForm;
