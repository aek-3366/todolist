import React from "react";
import { BiEdit } from "react-icons/bi";
import { MdDeleteForever } from "react-icons/md";
import Swal from "sweetalert2";
import { HiCheck } from "react-icons/hi";

function Todo({ task, deleteTodo, editTodo, toggleComplete }) {
  const alertdelte = (deletevalue) => {
    if (deletevalue) {
      Swal.fire("ลบข้อมูลสำเร็จ", "Successfully deleted data", "success");
      setTimeout(() => {
        Swal.close();
      }, 2000);
    }
  };

  return (
    <div className="Todo">
      <p
        className={`${task.completed ? "completed" : ""}`}
        onClick={() => toggleComplete(task.id)}
      >
        <div className="flex gap-2">
          <div>{task.task}</div>

          <HiCheck size={25} color="#66CC00" />
        </div>
      </p>
      <div style={{ display: "flex", gap: "5px" }}>
        <BiEdit
          size={30}
          onClick={() => editTodo(task.id)}
          className="hover:animate-pulse"
        />
        <MdDeleteForever
          size={30}
          onClick={() => {
            deleteTodo(task.id);
            alertdelte("delete");
          }}
          className="hover:animate-pulse"
        />
      </div>
    </div>
  );
}

export default Todo;
